import pandas as pd
import pickle


data = pd.read_pickle("data/Caltrain_city_center_v3.pkl")

traj_list = [group[["lons","lats"]].values for _,group in data.groupby("id_traj")]

pickle.dump(traj_list, open("data/" + "benchmark_trajectories.pkl","wb"))